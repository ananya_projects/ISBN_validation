package controllers

import javax.inject._
import play.api.mvc._
import models.Book
import play.api.libs.json._
import scala.collection.mutable


@Singleton class CatalogueController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  private val bookList = new mutable.ListBuffer[Book]()

  implicit val booksImplicitReads: Reads[Book] = Json.reads[Book]
  implicit val booksImplicitWrites: OWrites[Book] = Json.writes[Book]
  implicit val bookListJson: OFormat[Book] = Json.format[Book]

  // creating in-memory List of books
  bookList += Book("9780300267662", "Why Architecture Matters", "15")
  bookList += Book("9783110914672", "Reallexikon der deutschen Literaturwissenschaft", "27")
  bookList += Book("9783110914675", "The Death Penalty", "7")
  bookList += Book("9783110545982", "Qualitative Interviews", "33")
  bookList += Book("9780520392304", "Equality within Our Lifetimes", "19")
  bookList += Book("9780520392314", "A General Theory of Crime", "21")
  bookList += Book("9780300268478", "The Great New York Fire of 1776", "64")



  private def validateISBN(ISBN: String): Boolean = {
    var result: Boolean = false
    var mul : Int =0
    var checkDigit = 0

    if (ISBN.toList.size == 13) {
      val numList = ISBN.map(_.asDigit).toList.zipWithIndex
      checkDigit = numList.last._1
      numList.dropRight(1).foreach { case (char, idx) =>
        if ((idx+1) % 2 == 0)  mul += char*3
        else mul += char*1
      }
    }
//    println(mul, ISBN)
    val mod10Val = mul % 10
    if(mod10Val==0){ if(checkDigit ==0) result = true
    }
    else{ if(checkDigit==(10-mod10Val))  result = true
          else result = false
    }
    result
  }

  private  def convertAppendix(Appendix: String) : String = {
    val appendix_asInt = Appendix.toInt
//      .map(_.asDigit)
    var roman_numeral: String = ""
//    if(appendix_asInt > 99999) {
      println(appendix_asInt)
      val unit_List = (" ","I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX")
      val tens_List = (" ","X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC")
      val hundreds_List = (" ", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM")
      roman_numeral = "M" * (appendix_asInt / 1000) + hundreds_List.productElement((appendix_asInt % 1000) / 100) +
        tens_List.productElement((appendix_asInt % 100) / 10) + unit_List.productElement(appendix_asInt % 10)
      println(roman_numeral)
//    }
    roman_numeral
  }

  def getAllBooks: Action[AnyContent] = Action { implicit request: Request[AnyContent] => {

    if (bookList.isEmpty) {
      NoContent
    }
    else {
      val validBooks = new mutable.ListBuffer[Book]()

      for (book <- bookList) {
        println(book.isbn)
          val romanAppendix = convertAppendix(book.appendix)
          validBooks += Book(book.isbn, book.title, romanAppendix)
      }
      Ok(views.html.index(validBooks.toList))
    }
   }
  }

  def getBook(ISBN: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>{
    // validate ISBN
    if(validateISBN(ISBN)) {
      // check if ISBN is there is the list and fetch book
      val searchBook = bookList.find(_.isbn == ISBN)
      searchBook match {
        case Some(item) => {
          val roman = convertAppendix(item.appendix)
          val validBook = new Book(item.isbn, item.title, roman)
          Ok(views.html.catalogue(validBook))
        }
        case None => NotFound
      }
    }
    else Ok(Json.toJson("Invalid ISBN"))
    }
  }




  }