package models

import play.api.libs.json.{Json, OWrites, Reads}

case class Book(isbn: String, title: String, appendix: String)


object Book {
  implicit val booksImplicitReads: Reads[Book] = Json.reads[Book]
  implicit val booksImplicitWrites: OWrites[Book] = Json.writes[Book]
}
//case class TodoListItem(id: Long, description: String, isItDone: Boolean)
